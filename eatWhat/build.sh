#!/bin/bash

# 这个脚本的gitee.com/webhook请求
# 到宝塔的webhook, 然后执行的shell

ROOT_PATH=/root/project/eat-to-java/eatWhat
cd $ROOT_PATH

git pull

# mvn: 加上环境变量, 才能保证执行到
# -Dmaven.test.skip=true 是跳过测试代码
export M2_HOME=/opt/maven
export PATH=${M2_HOME}/bin:${PATH}
mvn clean
mvn package -Dmaven.test.skip=true

# 停止并删除之前运行的容器
docker stop myapp
docker rm myapp
docker rmi myapp

# 构建新的docker镜像
docker build -t myapp .

# 启动新的docker容器
docker run -d -p 9001:9001 --name myapp myapp
#docker run -d -p 9001:9001 -p 5005:5005 --name myapp myapp

# 查看容器状态
docker ps -a


#----
#!/bin/bash

# 这个脚本的gitee.com/webhook请求
# 到宝塔的webhook, 然后执行的shell

# 停止并删除之前运行的容器
#docker stop 8080-xxl-job-admin
#docker rm 8080-xxl-job-admin
#docker rmi 8080-xxl-job-admin
#
## 启动新的docker容器
#docker run -e PARAMS="--spring.datasource.url=jdbc:mysql://42.192.90.105:3306/xxl_job?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&serverTimezone=Asia/Shanghai --spring.datasource.username=root --spring.datasource.password=nidaye123.." -p 8080:8080 -v /tmp:/data/applogs --name 8080-xxl-job-admin xuxueli/xxl-job-admin:2.3.0
