package com.eat.what;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.eat.what.mapper")
public class EatSysApplication {

    public static void main(String[] args) {
        SpringApplication.run(EatSysApplication.class);
    }

}
