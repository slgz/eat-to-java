package com.eat.what.controller;


import cn.hutool.Hutool;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.eat.what.domain.CashOut;
import com.eat.what.domain.PoaUser;
import com.eat.what.service.PoaUserService;
import com.eat.what.utils.ASEUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private PoaUserService userService;

    @GetMapping("/get")
    public String getUser(@PathParam("name") String name){
//        redisCache.setCacheObject("test1", "1111");
//        redisCache.setCacheObject("test2", 2222);
//        redisCache.setCacheObject("test3", 3333, 130, TimeUnit.SECONDS);
//        System.out.println("test1: " + redisCache.getCacheObject("test1"));
//        System.out.println("test2: " + redisCache.getCacheObject("test2"));
//        System.out.println("test1: " + redisCache.getCacheObject("test3"));


        String now = DateUtil.now();

        return now;
    }

    @GetMapping("/cash")
    public String cashOut(CashOut cashOut) throws Exception {
        String host = "http://120.26.184.44:8080";
        String url = "/service/back/cash-out-submit.htm";
        String postUrl = host + url;

        // 获取参数
        String pmUid = ASEUtils.enIvEncrypt(cashOut.getPmUid());

        // 请求接口
        // 创建JSON对象
        JSONObject json = JSONUtil.createObj()
                .putOpt("pmUid", pmUid)
                .putOpt("amount", cashOut.getAmount())
                .putOpt("phone", cashOut.getAlipay())
                .putOpt("cardNo", cashOut.getCardNo())
                .putOpt("alipay", cashOut.getAlipay())
                .putOpt("fullName", cashOut.getFullName());

        // 发送POST请求
        assert json != null;
        String response = HttpUtil.createPost(postUrl)
                .contentType("application/json")
                .body(JSON.toJSONString(json))
                .timeout(5000) // 设置超时时间
                .execute()
                .body();

        // 输出响应结果
//        System.out.println(response);

        // TODO 修改用户手机号

        return response;
    }

}
