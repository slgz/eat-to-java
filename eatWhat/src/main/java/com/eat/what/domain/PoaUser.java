package com.eat.what.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户表
 * @TableName poa_user
 */
@TableName(value ="poa_user")
public class PoaUser implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long uid;

    /**
     * openId
     */
    private String openId;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 
     */
    private String photo;

    /**
     * 国家号
     */
    private String nationName;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 1:正常;90:停用;
     */
    private Integer status;

    /**
     * 用户性别，1表示男生，2表示女生
     */
    private Integer sex;

    /**
     * 用户年龄，-1表示未设置
     */
    private Integer age;

    /**
     * 链账户账号
     */
    private String chainAccount;

    /**
     * 用户链地址
     */
    private String chainOperationId;

    /**
     * 
     */
    private Date createTime;

    /**
     * 记录上次修改时间
     */
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public Long getUid() {
        return uid;
    }

    /**
     * 
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * openId
     */
    public String getOpenId() {
        return openId;
    }

    /**
     * openId
     */
    public void setOpenId(String openId) {
        this.openId = openId;
    }

    /**
     * 用户昵称
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * 用户昵称
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * 
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * 国家号
     */
    public String getNationName() {
        return nationName;
    }

    /**
     * 国家号
     */
    public void setNationName(String nationName) {
        this.nationName = nationName;
    }

    /**
     * 手机号
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 手机号
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 1:正常;90:停用;
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 1:正常;90:停用;
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 用户性别，1表示男生，2表示女生
     */
    public Integer getSex() {
        return sex;
    }

    /**
     * 用户性别，1表示男生，2表示女生
     */
    public void setSex(Integer sex) {
        this.sex = sex;
    }

    /**
     * 用户年龄，-1表示未设置
     */
    public Integer getAge() {
        return age;
    }

    /**
     * 用户年龄，-1表示未设置
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * 链账户账号
     */
    public String getChainAccount() {
        return chainAccount;
    }

    /**
     * 链账户账号
     */
    public void setChainAccount(String chainAccount) {
        this.chainAccount = chainAccount;
    }

    /**
     * 用户链地址
     */
    public String getChainOperationId() {
        return chainOperationId;
    }

    /**
     * 用户链地址
     */
    public void setChainOperationId(String chainOperationId) {
        this.chainOperationId = chainOperationId;
    }

    /**
     * 
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 记录上次修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 记录上次修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PoaUser other = (PoaUser) that;
        return (this.getUid() == null ? other.getUid() == null : this.getUid().equals(other.getUid()))
            && (this.getOpenId() == null ? other.getOpenId() == null : this.getOpenId().equals(other.getOpenId()))
            && (this.getNickName() == null ? other.getNickName() == null : this.getNickName().equals(other.getNickName()))
            && (this.getPhoto() == null ? other.getPhoto() == null : this.getPhoto().equals(other.getPhoto()))
            && (this.getNationName() == null ? other.getNationName() == null : this.getNationName().equals(other.getNationName()))
            && (this.getPhone() == null ? other.getPhone() == null : this.getPhone().equals(other.getPhone()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getSex() == null ? other.getSex() == null : this.getSex().equals(other.getSex()))
            && (this.getAge() == null ? other.getAge() == null : this.getAge().equals(other.getAge()))
            && (this.getChainAccount() == null ? other.getChainAccount() == null : this.getChainAccount().equals(other.getChainAccount()))
            && (this.getChainOperationId() == null ? other.getChainOperationId() == null : this.getChainOperationId().equals(other.getChainOperationId()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUid() == null) ? 0 : getUid().hashCode());
        result = prime * result + ((getOpenId() == null) ? 0 : getOpenId().hashCode());
        result = prime * result + ((getNickName() == null) ? 0 : getNickName().hashCode());
        result = prime * result + ((getPhoto() == null) ? 0 : getPhoto().hashCode());
        result = prime * result + ((getNationName() == null) ? 0 : getNationName().hashCode());
        result = prime * result + ((getPhone() == null) ? 0 : getPhone().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getSex() == null) ? 0 : getSex().hashCode());
        result = prime * result + ((getAge() == null) ? 0 : getAge().hashCode());
        result = prime * result + ((getChainAccount() == null) ? 0 : getChainAccount().hashCode());
        result = prime * result + ((getChainOperationId() == null) ? 0 : getChainOperationId().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uid=").append(uid);
        sb.append(", openId=").append(openId);
        sb.append(", nickName=").append(nickName);
        sb.append(", photo=").append(photo);
        sb.append(", nationName=").append(nationName);
        sb.append(", phone=").append(phone);
        sb.append(", status=").append(status);
        sb.append(", sex=").append(sex);
        sb.append(", age=").append(age);
        sb.append(", chainAccount=").append(chainAccount);
        sb.append(", chainOperationId=").append(chainOperationId);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}