package com.eat.what.service;

import com.eat.what.domain.PoaUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【poa_user(用户表)】的数据库操作Service
* @createDate 2023-08-31 16:24:06
*/
public interface PoaUserService extends IService<PoaUser> {

}
