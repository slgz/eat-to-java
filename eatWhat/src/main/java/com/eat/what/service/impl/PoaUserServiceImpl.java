package com.eat.what.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.eat.what.domain.PoaUser;
import com.eat.what.service.PoaUserService;
import com.eat.what.mapper.PoaUserMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【poa_user(用户表)】的数据库操作Service实现
* @createDate 2023-08-31 16:24:06
*/
@Service
public class PoaUserServiceImpl extends ServiceImpl<PoaUserMapper, PoaUser>
    implements PoaUserService{

}




