package com.eat.what.mapper;

import com.eat.what.domain.PoaUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【poa_user(用户表)】的数据库操作Mapper
* @createDate 2023-08-31 16:24:06
* @Entity generator.domain.PoaUser
*/
public interface PoaUserMapper extends BaseMapper<PoaUser> {

}




