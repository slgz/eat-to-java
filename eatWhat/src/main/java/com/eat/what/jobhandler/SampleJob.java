package com.eat.what.jobhandler;

import cn.hutool.core.date.DateUtil;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.slf4j.LoggerFactory;

/**
 * 测试xxl-job任务
 */
@Component
public class SampleJob {
    private static Logger logger = LoggerFactory.getLogger(SampleJob.class);


    @XxlJob("hello")
    public void helloJobHandler() throws Exception{
        XxlJobHelper.log("xxl-log, Hello World");

        String now = DateUtil.now();

        System.out.println(now + "===> hello world");

    }


}
